Multithreading in Java
Any application can have multiple process (instances). Each of this process can be assigned either as a single thread or multiple threads.

Single Thread: A single thread is basically a lightweight and the smallest unit of processing.
Multi-Thread: While multithreading can be defined as the execution of two or more threads concurrently.

What is Single Thread and Multithread
Threads
========================================
There are two types of thread � user thread and daemon thread (daemon threads are used when we want to clean the application and are used in the background).

When an application first begins, user thread is created. Post that, we can create many user threads and daemon threads.

Single Thread Example:

Advantages of single thread:

Reduces overhead in the application as single thread execute in the system
Also, it reduces the maintenance cost of the application.
Multithreads

Multithreaded applications are where two or more threads run concurrently. This multitasking is done, when multiple process shares common resources like CPU, memory, etc.

Each thread runs parallel to each other. Threads don't allocate separate memory area, hence it saves memory. Also, context switching between threads takes less time.

Example of Multi thread:

Advantages of multithread:

The users are not blocked because threads are independent, and we can perform multiple operations at times
As such the threads are independent, the other threads won't get affected if one thread meets an exception.

Thread life cycle in java
Life cycle of a thread:

image?

There are various stages in life cycle of thread as shown in above diagram:

New
Runnable
Running
Waiting
Dead


Some of commonly used methods for threads are:

start()
This method starts the execution of the thread and JVM calls the run() method on the thread.

We will cover this in the below example.

Sleep(int milliseconds)
This method makes the thread sleep hence the thread's execution will pause for milliseconds provided and after that, again the thread starts executing. This help in synchronization of the threads.

getName():
It returns the name of the thread.

setPriority(int newpriority)
It changes the priority of the thread.

yield ()
It causes current thread on halt and other threads to execute.

let's see an example:
=====================


Java Thread Synchronization
In multithreading, there is the asynchronous behavior of the programs. If one thread is writing some data and another thread which is reading data at the same time, might create inconsistency in the application.

When there is a need to access the shared resources by two or more threads, then synchronization approach is utilized.

Java has provided synchronized methods to implement synchronized behavior.

In this approach, once the thread reaches inside the synchronized block, then no other thread can call that method on the same object. All threads have to wait till that thread finishes the synchronized block and comes out of that.

In this way, the synchronization helps in a multithreaded application. One thread has to wait till other thread finishes its execution only then the other threads are allowed for execution.

It can be written in the following form:

Synchronized(object)
{  
        //Block of statements to be synchronized
}

let's see an example:
=====================

Tips:
Use synchronization to avoid unexpected results in a multi-thread environment, with a shared resource.

Try to use synchronized block over synchronized methods.

Use synchronization only if you know what you are doing. Do not lead the program to thread deadlock.

Do not use synchronization in a single thread application.

Do not use any thread-safe classes in a single thread applications. (Eg: Vector and StringBuffer)
